package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/golang-gin-poc/controllers"
	"gitlab.com/kokegudiel2/golang-gin-poc/middleware"
	"gitlab.com/kokegudiel2/golang-gin-poc/repository"
	"gitlab.com/kokegudiel2/golang-gin-poc/server"
	"gitlab.com/kokegudiel2/golang-gin-poc/service"
)

var (
	videoRepository repository.VideoRepository = repository.NewVideoRepository()

	videoService service.VideoService = service.New(videoRepository)
	jwtService   service.JWTService   = service.NewJWTService()
	loginService service.LoginService = service.NewLoginService()

	videoController controllers.VideoController = controllers.New(videoService)
	loginController controllers.LoginController = controllers.NewLoginController(loginService, jwtService)
)

func main() {
	//srv := gin.Default()
	srv := server.NewServer()

	srv.Static("/css", "./templates/css")
	srv.LoadHTMLGlob("templates/*.html")

	srv.POST("/login", func(ctx *gin.Context) {
		token := loginController.Login(ctx)
		if token != "" {
			ctx.JSON(http.StatusOK, gin.H{
				"token": token,
			})
		} else {
			ctx.JSON(http.StatusUnauthorized, nil)
		}
	})

	apiRoutes := srv.Group("/api", middleware.AuthorizeJWT())
	{
		apiRoutes.GET("/videos", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, videoController.FindAll())
		})

		apiRoutes.POST("/videos", func(ctx *gin.Context) {
			err := videoController.Save(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			} else {
				ctx.JSON(http.StatusCreated, gin.H{"msg": "Video input is valid"})
			}
		})

		apiRoutes.PUT("/videos/:id", func(ctx *gin.Context) {
			err := videoController.Update(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			} else {
				ctx.JSON(http.StatusAccepted, gin.H{"msg": "Video input is valid"})
			}
		})

		apiRoutes.DELETE("/videos/:id", func(ctx *gin.Context) {
			err := videoController.Delete(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, gin.H{"msg": "Video input is valid"})
			}
		})
	}

	viewRoutes := srv.Group("/view")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)
	}

	srv.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"messge": "tumadre.com",
		})
	})

	srv.ServerPort()
}
