package repository

import (
	"log"

	"gitlab.com/kokegudiel2/golang-gin-poc/entity"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type VideoRepository interface {
	Save(entity.Video)
	Update(entity.Video)
	Delete(entity.Video)
	FindAll() []entity.Video
}

type database struct {
	connection *gorm.DB
}

func NewVideoRepository() VideoRepository {
	db, err := gorm.Open(sqlite.Open("db.db"), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&entity.Video{}, &entity.Person{})

	return &database{db}
}

func (d *database) Save(v entity.Video) {
	d.connection.Create(&v)
}

func (d *database) Update(v entity.Video) {
	d.connection.Save(&v)
}

func (d *database) Delete(v entity.Video) {
	d.connection.Delete(&v)
}

func (d *database) FindAll() []entity.Video {
	var videos []entity.Video
	err := d.connection.Model(&entity.Video{}).Preload("Person").Find(&videos).Error
	log.Println("FindAll error", err)

	return videos
}
