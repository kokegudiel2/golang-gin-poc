package server

import (
	"io"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/golang-gin-poc/middleware"
)

type BasicServer struct {
	*gin.Engine
}

func setupLogOutput() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func NewServer() *BasicServer {
	srv := gin.New()

	setupLogOutput()

	srv.Use(gin.Recovery(), middleware.Logger())

	return &BasicServer{srv}
}

func (s *BasicServer) ServerPort() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8484"
	}

	s.Run(":" + port)
}
