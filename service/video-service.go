package service

import (
	"gitlab.com/kokegudiel2/golang-gin-poc/entity"
	"gitlab.com/kokegudiel2/golang-gin-poc/repository"
)

type VideoService interface {
	Save(entity.Video) entity.Video
	FindAll() []entity.Video
	Update(entity.Video)
	Delete(entity.Video)
}

type videoService struct {
	videoRepository repository.VideoRepository
}

func New(videoRepository repository.VideoRepository) VideoService {
	return &videoService{videoRepository}
}

func (v *videoService) Save(video entity.Video) entity.Video {
	v.videoRepository.Save(video)
	return video
}

func (v *videoService) FindAll() []entity.Video {
	return v.videoRepository.FindAll()
}

func (v *videoService) Update(video entity.Video) {
	v.videoRepository.Update(video)
}

func (v *videoService) Delete(video entity.Video) {
	v.videoRepository.Delete(video)
}
