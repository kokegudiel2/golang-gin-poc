package controllers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/kokegudiel2/golang-gin-poc/entity"
	"gitlab.com/kokegudiel2/golang-gin-poc/service"
	"gitlab.com/kokegudiel2/golang-gin-poc/validators"
)

var validate *validator.Validate

type VideoController interface {
	FindAll() []entity.Video
	Save(ctx *gin.Context) error
	Update(ctx *gin.Context) error
	Delete(ctx *gin.Context) error
	ShowAll(ctx *gin.Context)
}

type controller struct {
	service service.VideoService
}

func New(service service.VideoService) VideoController {
	validate = validator.New()
	validate.RegisterValidation("is-cool", validators.ValidatorCoolTitle)
	return controller{
		service: service,
	}
}

func (c controller) FindAll() []entity.Video {
	v := c.service.FindAll()
	if v[0].Author.Firstname == "" {
		log.Println("No pasa datos", v[0].Author)
	}

	return v
}

func (c controller) Save(ctx *gin.Context) error {
	var video entity.Video
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return err
	}

	err = validate.Struct(video)
	if err != nil {
		return err
	}

	c.service.Save(video)

	return nil
}

func (c controller) ShowAll(ctx *gin.Context) {
	videos := c.service.FindAll()
	data := gin.H{
		"title":  "Video Page",
		"videos": videos,
	}

	ctx.HTML(http.StatusOK, "index.html", data)
}

func (c controller) Update(ctx *gin.Context) error {
	var video entity.Video
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 10, 0)
	if err != nil {
		return err
	}
	video.ID = id

	err = validate.Struct(video)
	if err != nil {
		return err
	}

	c.service.Update(video)

	return nil
}

func (c controller) Delete(ctx *gin.Context) error {
	var video entity.Video

	id, err := strconv.ParseUint(ctx.Param("id"), 10, 0)
	if err != nil {
		return err
	}
	video.ID = id

	err = validate.Struct(video)
	if err != nil {
		return err
	}

	c.service.Delete(video)

	return nil
}
