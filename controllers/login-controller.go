package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/golang-gin-poc/dto"
	"gitlab.com/kokegudiel2/golang-gin-poc/service"
)

type LoginController interface {
	Login(ctx *gin.Context) string
}

type loginController struct {
	loginService service.LoginService
	jwtService   service.JWTService
}

func NewLoginController(s service.LoginService, j service.JWTService) LoginController {
	return &loginController{
		loginService: s,
		jwtService:   j,
	}
}

func (l *loginController) Login(ctx *gin.Context) string {
	var credential dto.Credentials
	err := ctx.ShouldBind(&credential)
	if err != nil {
		return ""
	}

	isAuthenticated := l.loginService.Login(credential.Username, credential.Password)
	if isAuthenticated {
		return l.jwtService.GenerateToken(credential.Username, true)
	}
	return ""
}
