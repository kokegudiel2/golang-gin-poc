package middleware

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

func Logger() gin.HandlerFunc {
	return gin.LoggerWithFormatter(func(params gin.LogFormatterParams) string {
		return fmt.Sprintln(params.ClientIP, params.TimeStamp.Format(time.RFC822), params.Method, params.Path, params.StatusCode, params.Latency)
	})
}
